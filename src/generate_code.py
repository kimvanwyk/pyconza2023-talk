from jinja2 import Environment, FileSystemLoader, select_autoescape
from rich import print
import rich_click as click

import pathlib
import db


ENV = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape(),
)


def render_model_file(output_dir, rows, debug):
    d = {"models": {}}
    for name, values in rows.items():
        d["models"][name] = {}
        for direction in ("in", "out"):
            d["models"][name][direction] = (
                list(values[direction].items()) if values[direction] else None
            )
    if debug:
        print(d)
    for path in ("models.py", "main.py"):
        template = ENV.get_template(path)
        outfile = pathlib.Path(output_dir).joinpath(path)
        with open(outfile, "w") as fh:
            fh.write(template.render(d))


@click.command()
@click.argument(
    "output_dir",
    type=click.Path(exists=False),
)
@click.option(
    "-v",
    "--verbose",
    default=False,
    is_flag=True,
    flag_value=True,
    help="Whether to output debug",
)
def main(output_dir, verbose=False):
    rows = db.get_proc_attributes()
    if verbose:
        print(rows)
    render_model_file(output_dir, rows, verbose)


if __name__ == "__main__":
    main()
