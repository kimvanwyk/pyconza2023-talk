from collections import defaultdict
import sqlite3

DTYPE_MAPPINGS = {
    "bit": "bool",
    "datetime": "datetime.datetime",
    "float": "float",
    "decimal": "float",
    "numeric": "float",
    "varchar": "str",
    "xml": "str",
    "uniqueidentifier": "str",
    "int": "int",
}


def get_proc_attributes():
    conn = sqlite3.connect("db.sqlite3")
    c = conn.cursor()
    c.execute("SELECT * FROM sp")
    d = defaultdict(lambda: {"in": {}, "out": {}})
    for row in c:
        d[row[0]]["out" if row[3] else "in"][row[1][1:]] = DTYPE_MAPPINGS[row[2]]
    return d


if __name__ == "__main__":
    d = get_proc_attributes()
    print(d)
