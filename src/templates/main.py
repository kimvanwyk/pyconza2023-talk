from fastapi import FastAPI, HTTPException
from rich import print

import models

import os

app = FastAPI()

{% for (name, mods) in models.items() %}
@app.post("/{{ name }}")
def {{ name }}({% if mods["in"] %}input_model: models.{{ name }}InputModel{% endif %}):
    {% if not mods["in"] %}input_model = None{% endif %}
    output_model = {% if mods["out"] %}models.{{ name }}OutputModel(){% else %}None{% endif %}
    try:
        om = models.{{ name }}OutputModel()
    except Exception as e:
        raise HTTPException(status_code = 400, detail = str(e))
    return om
{% endfor %}
