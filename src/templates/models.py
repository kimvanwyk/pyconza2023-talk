from pydantic import BaseModel
import datetime

{% for (name,mods) in models.items() %}{% for (direction, params) in mods.items() %}{% if params %}
class {{ name }}{% if direction == "in" %}InputModel{% else %}OutputModel{% endif %}(BaseModel):
    {% for (field, tp) in params %}{{ field }}: {{ tp }}{% if direction == "out" %} = {% if tp == "str" %}"Exciting String"{% else %}42.0{% endif %}{% endif %}
    {% endfor %}
{% endif %}{% endfor %}{% endfor %}
